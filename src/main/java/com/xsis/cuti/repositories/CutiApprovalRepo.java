package com.xsis.cuti.repositories;

import java.util.Optional;

import com.xsis.cuti.models.Cuti;
import com.xsis.cuti.models.CutiApproval;
import com.xsis.cuti.models.Karyawan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface CutiApprovalRepo extends JpaRepository<CutiApproval, Long> {
  @Query("SELECT c FROM Cuti c WHERE Id = ?1")
  Optional<Cuti> findCutiById(Long id);

  @Query("SELECT c FROM Karyawan c WHERE Id = ?1")
  Optional<Karyawan> findKaryawanById(Long id);

  @Modifying
  @Transactional
  @Query("UPDATE Karyawan k SET k.sisaCuti = ?2 WHERE Id = ?1")
  void updateSisaCutiKaryawan(Long id, Integer lamaCuti);
}
