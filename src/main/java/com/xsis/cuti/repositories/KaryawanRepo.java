package com.xsis.cuti.repositories;

import com.xsis.cuti.models.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface KaryawanRepo extends JpaRepository<Karyawan, Long> {
    @Query("FROM Karyawan WHERE jenisKaryawanId = ?1")
    List<Karyawan> FindByJenisKaryawanId(Long jenisKaryawanId);

    @Query(value = "SELECT * FROM Karyawan e WHERE e.email = ?1 AND e.password = ?2", nativeQuery = true)
    Optional<Karyawan> findByEmailPassword(String email, String password);

}
