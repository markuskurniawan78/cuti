package com.xsis.cuti.repositories;

import java.util.List;

import com.xsis.cuti.models.JenisKaryawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface JenisKaryawanRepo extends JpaRepository<JenisKaryawan, Long> {
  @Query("FROM JenisKaryawan WHERE lower(nama) LIKE lower(concat('%',?1,'%') ) ")
  List<JenisKaryawan> SearchJenisKaryawan(String keyword);
}
