package com.xsis.cuti.repositories;

import java.util.List;

import com.xsis.cuti.models.JenisCuti;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface JenisCutiRepo extends JpaRepository<JenisCuti, Long> {
  @Query("FROM JenisCuti WHERE lower(nama) LIKE lower(concat('%',?1,'%') ) ")
  List<JenisCuti> SearchJenisCuti(String keyword);
}
