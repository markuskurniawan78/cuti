package com.xsis.cuti.repositories;

import com.xsis.cuti.models.JenisKaryawan;
import com.xsis.cuti.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StatusRepo extends JpaRepository<Status, Long> {
    @Query("FROM Status WHERE lower(nama) LIKE lower(concat('%',?1,'%') ) ")
    List<Status> SearchStatus(String keyword);
}
