package com.xsis.cuti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CutiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CutiApplication.class, args);
	}

}
