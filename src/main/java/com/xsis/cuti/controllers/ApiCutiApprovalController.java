package com.xsis.cuti.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import com.xsis.cuti.models.Cuti;
import com.xsis.cuti.models.CutiApproval;
import com.xsis.cuti.models.Karyawan;
import com.xsis.cuti.repositories.CutiApprovalRepo;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCutiApprovalController {

  @Autowired
  private CutiApprovalRepo cutiApprovalRepo;

  @PostMapping("/cuti_approval")
  public ResponseEntity<Object> saveCutiApproval(@RequestBody CutiApproval cutiApproval, @PathVariable("id") Long id,
      HttpSession session) {
    try {
      Optional<CutiApproval> cutiApprovalData = this.cutiApprovalRepo.findById(id);

      // Optional<Cuti> cuti = this.cutiApprovalRepo.findCutiById(id);
      // Optional<Karyawan> karyawan =
      // this.cutiApprovalRepo.findKaryawanById(cuti.get().getKaryawanId());

      if (cutiApprovalData.isPresent()) {
        // if (cutiApproval.getStatusId() == 3) {
        // int sisacuti = karyawan.get().getSisaCuti() + cuti.get().getLamaCuti();
        // // System.out.println("====================="+sisacuti);
        // this.cutiApprovalRepo.updateSisaCutiKaryawan((Long)
        // session.getAttribute("id"), sisacuti);
        // }
        cutiApproval.setCreatedBy("markus");
        cutiApproval.setCreatedOn(new Date());
        this.cutiApprovalRepo.save(cutiApproval);
        return new ResponseEntity<>("success", HttpStatus.OK);
      } else {
        return ResponseEntity.notFound().build();
      }
    } catch (Exception exception) {
      return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping("/cuti_approval")
  public ResponseEntity<List<CutiApproval>> GetAllCutiApproval() {
    try {
      List<CutiApproval> cutiApprovals = this.cutiApprovalRepo.findAll();
      return new ResponseEntity<>(cutiApprovals, HttpStatus.OK);
    } catch (Exception exception) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
  }

  @GetMapping("/cuti_approval/{id}")
  public ResponseEntity<List<CutiApproval>> GetCutiApprovalById(@PathVariable("id") Long Id) {
    try {
      Optional<CutiApproval> cutiApproval = this.cutiApprovalRepo.findById(Id);
      if (cutiApproval.isPresent()) {
        ResponseEntity rest = new ResponseEntity<>(cutiApproval, HttpStatus.OK);
        return rest;
      } else {
        return ResponseEntity.notFound().build();
      }
    } catch (Exception exception) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
  }

}
