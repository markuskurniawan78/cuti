package com.xsis.cuti.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
  @Autowired
  private JdbcTemplate jdbcTemplate;
  NamedParameterJdbcTemplate template;

  @GetMapping(value = "/login")
  public ModelAndView index() {
    ModelAndView view = new ModelAndView("login/index");
    return view;
  }

}
