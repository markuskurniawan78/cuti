package com.xsis.cuti.controllers;

import com.xsis.cuti.models.JenisCuti;
import com.xsis.cuti.repositories.JenisCutiRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiJenisCutiController {

    @Autowired
    private JenisCutiRepo jenisCutiRepo;

    @PostMapping(value = "/jenis_cuti")
    public ResponseEntity<Object> SaveJenisCuti(@RequestBody JenisCuti jenisCuti) {
        try {
            jenisCuti.setCreatedBy("markus");
            jenisCuti.setCreatedOn(new Date());
            this.jenisCutiRepo.save(jenisCuti);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/jenis_cuti")
    public ResponseEntity<List<JenisCuti>> GetAllJenisCuti() {
        try {
            List<JenisCuti> jenisCutiList = this.jenisCutiRepo.findAll();
            return new ResponseEntity<>(jenisCutiList, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/jenis_cuti/{id}")
    public ResponseEntity<List<JenisCuti>> GetJenisCutiById(@PathVariable("id") Long id) {
        try {
            Optional<JenisCuti> jenisCuti = this.jenisCutiRepo.findById(id);
            if (jenisCuti.isPresent()) {
                // buat entity untuk return
                ResponseEntity rest = new ResponseEntity(jenisCuti, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/jenis_cuti/{id}")
    public ResponseEntity<Object> UpdateJenisCuti(@RequestBody JenisCuti jenisCuti, @PathVariable("id") Long id) {
        try {
            Optional<JenisCuti> jenisCutiData = this.jenisCutiRepo.findById(id);

            if (jenisCutiData.isPresent()) {
                jenisCuti.setId(id);
                jenisCuti.setModifiedBy("markus");
                jenisCuti.setModifiedOn(new Date());
                this.jenisCutiRepo.save(jenisCuti);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/jenis_cuti/{id}")
    public ResponseEntity<Object> DeleteJenisCuti(@RequestBody JenisCuti jenisCuti, @PathVariable("id") Long id) {
        try {
            Optional<JenisCuti> jenisCutiData = this.jenisCutiRepo.findById(id);

            if (jenisCutiData.isPresent()) {
                jenisCuti.setId(id);
                jenisCuti.setModifiedBy("markus");
                jenisCuti.setModifiedOn(new Date());
                jenisCuti.setDelete(true);
                this.jenisCutiRepo.save(jenisCuti);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("searchjeniscuti/{keyword}")
    public ResponseEntity<List<JenisCuti>> SearchNamaJenisCuti(@PathVariable("keyword") String keyword) {

        if (keyword != null) {
            List<JenisCuti> jeniscuti = this.jenisCutiRepo.SearchJenisCuti(keyword);
            return new ResponseEntity<>(jeniscuti, HttpStatus.OK);
        } else {
            List<JenisCuti> jeniscuti = this.jenisCutiRepo.findAll();
            return new ResponseEntity<>(jeniscuti, HttpStatus.OK);
        }

    }

}
