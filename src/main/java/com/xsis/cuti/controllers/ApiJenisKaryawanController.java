package com.xsis.cuti.controllers;

import com.xsis.cuti.models.JenisCuti;
import com.xsis.cuti.models.JenisKaryawan;
import com.xsis.cuti.models.Karyawan;
import com.xsis.cuti.repositories.JenisCutiRepo;
import com.xsis.cuti.repositories.JenisKaryawanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiJenisKaryawanController {
    @Autowired
    private JenisKaryawanRepo jenisKaryawanRepo;

    @PostMapping(value = "/jenis_karyawan")
    public ResponseEntity<Object> SaveJenisKaryawan(@RequestBody JenisKaryawan jenisKaryawan) {
        try {
            jenisKaryawan.setCreatedBy("markus");
            jenisKaryawan.setCreatedOn(new Date());
            this.jenisKaryawanRepo.save(jenisKaryawan);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/jenis_karyawan")
    public ResponseEntity<List<JenisKaryawan>> GetAllJenisKaryawan() {
        try {
            List<JenisKaryawan> jenisKaryawanList = this.jenisKaryawanRepo.findAll();
            return new ResponseEntity<>(jenisKaryawanList, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/jenis_karyawan/{id}")
    public ResponseEntity<List<JenisKaryawan>> GetJenisKaryawanById(@PathVariable("id") Long id) {
        try {
            Optional<JenisKaryawan> jenisKaryawan = this.jenisKaryawanRepo.findById(id);
            if (jenisKaryawan.isPresent()) {
                // buat entity untuk return
                ResponseEntity rest = new ResponseEntity(jenisKaryawan, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/jenis_karyawan/{id}")
    public ResponseEntity<Object> UpdateJenisKaryawan(@RequestBody JenisKaryawan jenisKaryawan,
            @PathVariable("id") Long id) {
        try {
            Optional<JenisKaryawan> jenisKaryawanData = this.jenisKaryawanRepo.findById(id);

            if (jenisKaryawanData.isPresent()) {
                jenisKaryawan.setId(id);
                jenisKaryawan.setModifiedBy("markus");
                jenisKaryawan.setModifiedOn(new Date());
                this.jenisKaryawanRepo.save(jenisKaryawan);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/jenis_karyawan/{id}")
    public ResponseEntity<Object> DeleteJenisKaryawan(@RequestBody JenisKaryawan jenisKaryawan,
            @PathVariable("id") Long id) {
        try {
            Optional<JenisKaryawan> jenisKaryawanData = this.jenisKaryawanRepo.findById(id);

            if (jenisKaryawanData.isPresent()) {
                jenisKaryawan.setId(id);
                jenisKaryawan.setModifiedBy("markus");
                jenisKaryawan.setModifiedOn(new Date());
                jenisKaryawan.setDelete(true);
                this.jenisKaryawanRepo.save(jenisKaryawan);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("searchjeniskaryawan/{keyword}")
    public ResponseEntity<List<JenisKaryawan>> SearchNamaJenisKaryawan(@PathVariable("keyword") String keyword) {

        if (keyword != null) {
            List<JenisKaryawan> jenisKaryawan = this.jenisKaryawanRepo.SearchJenisKaryawan(keyword);
            return new ResponseEntity<>(jenisKaryawan, HttpStatus.OK);
        } else {
            List<JenisKaryawan> jenisKaryawan = this.jenisKaryawanRepo.findAll();
            return new ResponseEntity<>(jenisKaryawan, HttpStatus.OK);
        }

    }

}
