package com.xsis.cuti.controllers;

import com.xsis.cuti.models.JenisCuti;
import com.xsis.cuti.models.JenisKaryawan;
import com.xsis.cuti.models.Status;
import com.xsis.cuti.repositories.JenisCutiRepo;
import com.xsis.cuti.repositories.StatusRepo;
import org.apache.logging.log4j.status.StatusData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiStatusController {
    @Autowired
    private StatusRepo statusRepo;

    @PostMapping(value = "/status")
    public ResponseEntity<Object> SaveStatus(@RequestBody Status status) {
        try {
            status.setCreatedBy("markus");
            status.setCreatedOn(new Date());
            this.statusRepo.save(status);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/status")
    public ResponseEntity<List<Status>> GetAllStatus() {
        try {
            List<Status> statusList = this.statusRepo.findAll();
            return new ResponseEntity<>(statusList, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/status/{id}")
    public ResponseEntity<List<Status>> GetStatusById(@PathVariable("id") Long id) {
        try {
            Optional<Status> status = this.statusRepo.findById(id);
            if (status.isPresent()) {
                // buat entity untuk return
                ResponseEntity rest = new ResponseEntity(status, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/status/{id}")
    public ResponseEntity<Object> UpdateStatus(@RequestBody Status status, @PathVariable("id") Long id) {
        try {
            Optional<Status> statusData = this.statusRepo.findById(id);

            if (statusData.isPresent()) {
                status.setId(id);
                status.setModifiedBy("markus");
                status.setModifiedOn(new Date());
                this.statusRepo.save(status);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/status/{id}")
    public ResponseEntity<Object> DeleteStatus(@RequestBody Status status, @PathVariable("id") Long id) {
        try {
            Optional<Status> statusData = this.statusRepo.findById(id);

            if (statusData.isPresent()) {
                status.setId(id);
                status.setModifiedBy("markus");
                status.setModifiedOn(new Date());
                status.setDelete(true);
                this.statusRepo.save(status);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("searchstatus/{keyword}")
    public ResponseEntity<List<Status>> SearchStatus(@PathVariable("keyword") String keyword) {

        if (keyword != null) {
            List<Status> status = this.statusRepo.SearchStatus(keyword);
            return new ResponseEntity<>(status, HttpStatus.OK);
        } else {
            List<Status> status = this.statusRepo.findAll();
            return new ResponseEntity<>(status, HttpStatus.OK);
        }

    }

}
