package com.xsis.cuti.controllers;

import com.xsis.cuti.models.Cuti;
import com.xsis.cuti.models.Karyawan;
import com.xsis.cuti.repositories.CutiRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCutiController {

    @Autowired
    private CutiRepo cutiRepo;

    @PostMapping("/cuti")
    public ResponseEntity<Object> SaveCuti(@RequestBody Cuti cuti, HttpSession session) {
        try {
            Optional<Karyawan> karyawan = this.cutiRepo.findKaryawanById((Long) session.getAttribute("id"));
            if (karyawan.get().getSisaCuti() > cuti.getLamaCuti()) {
                int sisaCuti = karyawan.get().getSisaCuti() - cuti.getLamaCuti();
                this.cutiRepo.updateSisaCutiKaryawan((Long) session.getAttribute("id"), sisaCuti);
                cuti.setCreatedBy("markus");
                cuti.setCreatedOn(new Date());
                this.cutiRepo.save(cuti);
                return new ResponseEntity<>("success", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("sisa cuti kurang/habis", HttpStatus.OK);
            }
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/cuti")
    public ResponseEntity<List<Cuti>> GetAllCuti() {
        try {
            List<Cuti> cuti = this.cutiRepo.findAll();
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/cuti/{id}")
    public ResponseEntity<List<Cuti>> GetCutiById(@PathVariable("id") Long id) {
        try {
            Optional<Cuti> cuti = this.cutiRepo.findById(id);
            if (cuti.isPresent()) {
                // buat entity untuk return
                ResponseEntity rest = new ResponseEntity(cuti, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/cuti/{id}")
    public ResponseEntity<Object> UpdateCuti(@RequestBody Cuti cuti, @PathVariable("id") Long id) {
        try {
            Optional<Cuti> cutiData = this.cutiRepo.findById(id);

            if (cutiData.isPresent()) {
                cuti.setId(id);
                cuti.setModifiedBy("markus");
                cuti.setModifiedOn(new Date());
                this.cutiRepo.save(cuti);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/cuti/{id}")
    public ResponseEntity<Object> DeleteCuti(@RequestBody Cuti cuti, @PathVariable("id") Long id, HttpSession session) {
        try {
            Optional<Cuti> cutiData = this.cutiRepo.findById(id);
            Optional<Karyawan> karyawan = this.cutiRepo.findKaryawanById((Long) session.getAttribute("id"));

            if (cutiData.isPresent()) {
                int sisaCuti = karyawan.get().getSisaCuti() + cutiData.get().getLamaCuti();
                this.cutiRepo.updateSisaCutiKaryawan((Long) session.getAttribute("id"), sisaCuti);
                cuti.setId(id);
                cuti.setModifiedBy("markus");
                cuti.setModifiedOn(new Date());
                cuti.setDelete(true);
                this.cutiRepo.save(cuti);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

}
