package com.xsis.cuti.controllers;

import com.xsis.cuti.models.Karyawan;
import com.xsis.cuti.models.Login;
import com.xsis.cuti.repositories.KaryawanRepo;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiKaryawanController {

    @Autowired
    private KaryawanRepo karyawanRepo;

    @PostMapping(value = "/karyawan")
    public ResponseEntity<Object> SaveKaryawan(@RequestBody Karyawan karyawan) {
        try {
            karyawan.setCreatedBy("markus");
            karyawan.setCreatedOn(new Date());
            this.karyawanRepo.save(karyawan);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/karyawan")
    public ResponseEntity<List<Karyawan>> GetAllKaryawan() {
        try {
            List<Karyawan> karyawans = this.karyawanRepo.findAll();
            return new ResponseEntity<>(karyawans, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/karyawan/{id}")
    public ResponseEntity<List<Karyawan>> GetKaryawanById(@PathVariable("id") Long id) {
        try {
            Optional<Karyawan> karyawan = this.karyawanRepo.findById(id);
            if (karyawan.isPresent()) {
                // buat entity untuk return
                ResponseEntity rest = new ResponseEntity(karyawan, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/karyawan/{id}")
    public ResponseEntity<Object> UpdateKaryawan(@RequestBody Karyawan karyawan, @PathVariable("id") Long id) {
        try {
            Optional<Karyawan> karyawanData = this.karyawanRepo.findById(id);

            if (karyawanData.isPresent()) {
                karyawan.setId(id);
                karyawan.setModifiedBy("markus");
                karyawan.setModifiedOn(new Date());
                this.karyawanRepo.save(karyawan);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/karyawan/{id}")
    public ResponseEntity<Object> DeleteKaryawan(@RequestBody Karyawan karyawan, @PathVariable("id") Long id) {
        try {
            Optional<Karyawan> categoryData = this.karyawanRepo.findById(id);

            if (categoryData.isPresent()) {
                karyawan.setId(id);
                karyawan.setModifiedBy("markus");
                karyawan.setModifiedOn(new Date());
                karyawan.setDelete(true);
                this.karyawanRepo.save(karyawan);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/validate")
    public ResponseEntity<Object> ValidateKaryawan(@RequestBody Login login, HttpSession session) {
        try {
            Optional<Karyawan> karyawanData = this.karyawanRepo.findByEmailPassword(login.getEmail(),
                    login.getPassword());
            if (karyawanData.isPresent()) {
                session.setAttribute("id", karyawanData.get().getId());
                session.setAttribute("jenisKaryawanId", karyawanData.get().getJenisKaryawanId());
                session.setAttribute("nama", karyawanData.get().getNama());
                return new ResponseEntity<>("success", HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/logout")
    public ResponseEntity<Object> LogoutKaryawan(HttpSession session) {
        try {

            if (session.getAttribute("id") != null && session.getAttribute("jenisKaryawanId") != null) {
                session.removeAttribute("id");
                session.removeAttribute("jenisKaryawanId");
                session.removeAttribute("nama");
                return new ResponseEntity<>("logout success", HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>("logout failed", HttpStatus.BAD_REQUEST);
        }
    }

}
