package com.xsis.cuti.models;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Where(clause = "is_delete = false")
@Table(name = "karyawan")

public class Karyawan extends Common{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "nama")
    private String nama;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToOne
    @JoinColumn(name = "jenis_karyawan_id", insertable = false, updatable = false)
    public JenisKaryawan jenisKaryawan;

    @Column(name = "jenis_karyawan_id", nullable = false)
    private Long jenisKaryawanId;

    @Column(name = "sisa_cuti")
    private int sisaCuti = 12;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public JenisKaryawan getJenisKaryawan() {
        return jenisKaryawan;
    }

    public void setJenisKaryawan(JenisKaryawan jenisKaryawan) {
        this.jenisKaryawan = jenisKaryawan;
    }

    public Long getJenisKaryawanId() {
        return jenisKaryawanId;
    }

    public void setJenisKaryawanId(Long jenisKaryawanId) {
        this.jenisKaryawanId = jenisKaryawanId;
    }

    public int getSisaCuti() {
        return sisaCuti;
    }

    public void setSisaCuti(int sisaCuti) {
        this.sisaCuti = sisaCuti;
    }
}
