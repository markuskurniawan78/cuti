package com.xsis.cuti.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Where(clause = "status_id != 3")
@Table(name = "cuti_approval")
public class CutiApproval extends Common {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long Id;

  @ManyToOne
  @JoinColumn(name = "jenis_cuti_id", insertable = false, updatable = false)
  public JenisCuti jenisCuti;

  @Column(name = "jenis_cuti_id", nullable = true)
  private Long JenisCutiId;

  @ManyToOne
  @JoinColumn(name = "status_id", insertable = false, updatable = false)
  public Status status;

  @Column(name = "status_id", nullable = true)
  private Long StatusId;

  @Column(name = "keterangan", nullable = false)
  private String keterangan;

  public Long getId() {
    return Id;
  }

  public void setId(Long id) {
    Id = id;
  }

  public JenisCuti getJenisCuti() {
    return jenisCuti;
  }

  public void setJenisCuti(JenisCuti jenisCuti) {
    this.jenisCuti = jenisCuti;
  }

  public Long getJenisCutiId() {
    return JenisCutiId;
  }

  public void setJenisCutiId(Long jenisCutiId) {
    JenisCutiId = jenisCutiId;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public Long getStatusId() {
    return StatusId;
  }

  public void setStatusId(Long statusId) {
    StatusId = statusId;
  }

  public String getKeterangan() {
    return keterangan;
  }

  public void setKeterangan(String keterangan) {
    this.keterangan = keterangan;
  }

}
